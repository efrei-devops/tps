# retrieve dynamically the latest version of the AMI WebApp we built with Packer
data "aws_ami" "web_ami" {
  most_recent = true     # the most recent one
  owners      = ["self"] # your own images
  name_regex  = "^WebApp"
}

# displays details of the AMI in terraform plan/apply commands
output "web_ami" {
  value = {
    id   = data.aws_ami.web_ami.id
    name = data.aws_ami.web_ami.name
  }
}
