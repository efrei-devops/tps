#!/bin/bash -e

sudo apt update && \
sudo apt install software-properties-common --yes && \
sudo apt-add-repository --update ppa:ansible/ansible --yes && \
sudo apt install ansible --yes && \
ansible --version && \
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add - && \
sudo apt-add-repository -y "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main" && \
sudo apt-get update && sudo apt-get install -y packer && \
packer -v
